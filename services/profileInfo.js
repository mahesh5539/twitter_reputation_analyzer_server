var twitterdata = require('./twitterData');

function getProfileData_handle_request(msg, callback){	
	try{
		var res = {};
		var screen_name = msg.twitter_handle;
		
				
		twitterdata.fetchTwitterProfileData(function(err,results){
			try{
				if (!err){
					  res.code = "200";
					  res.value = results;
					  callback(null, res);
				  }
				  else{
					  console.log("error while fetching user profile details.");
					  res.code = "500";
					  res.value = {"result":"", "error": true, "message": err.message};		
					  callback(null, res);
				  }
			} catch(err){
				res.code = "500";
				res.value = {"result":"", "error": true, "message": err.message};
				callback(null, res);
			}  
		},screen_name);	
	}
	catch(ex){
		var res = {};
		console.log(ex.message);
		res.code = "500";
		res.value = {"result":"", "error": true, "message": ex.message};
		callback(null, res);
	}
}

function getAllTweets_handle_request(msg, callback){
	console.log("1");
	try{
		var res = {tweetArray : []};
		var screen_name = msg.twitter_handle;		
		twitterdata.fetchAllTweets(function(err,results){
			console.log("2");

			try{
				
				
				var jsonStr;
				if (!err){
					  res.code = "200";
					  for(var i=0;i<results.length;i++){
					   var dirtyJsonStr = results[i].text;
						   
						  var index = dirtyJsonStr.indexOf("http");
						  var cleanJsonStrg = dirtyJsonStr.slice(0,index-1);
						  if(results[i].entities.media== undefined){
							  res.tweetArray.push({
								  "tweet":cleanJsonStrg,
								  "retweet_count":results[i].retweet_count,
								   "media":"without_photo",
								   "date":results[i].created_at,
								   "media_image_url":"null"
							  });
							   
						  }
						  else{
							 
							  res.tweetArray.push({
								  "tweet":cleanJsonStrg,
								  "retweet_count":results[i].retweet_count,
								  "media":results[i].entities.media[0].type,
								  "date":results[i].created_at,
								   "media_image_url":results[i].entities.media[0].media_url
							  });
							  							  							  
							 
							  
						  }						
					  }
					
					  callback(null, res);
				  }
				  else{
					  console.log("error while fetching user profile details.");
					  res.code = "500";
					  res.value = {"result":"", "error": true, "message": err.message};		
					  callback(null, res);
				  }
			} catch(err){
				res.code = "500";
				res.value = {"result":"", "error": true, "message": err.message};
				callback(null, res);
			}  
		},screen_name);	
	}
	catch(ex){
		var res = {};
		console.log(ex.message);
		res.code = "500";
		res.value = {"result":"", "error": true, "message": ex.message};
		callback(null, res);
	}
}
exports.getProfileData_handle_request = getProfileData_handle_request;
exports.getAllTweets_handle_request = getAllTweets_handle_request;