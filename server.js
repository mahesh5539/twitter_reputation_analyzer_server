
var amqp = require('amqp')
, util = require('util');
var profileInfo = require('./services/profileInfo'); 	



var cnn = amqp.createConnection({host:'127.0.0.1'});

cnn.on('ready', function(){
	console.log("listening on profile_queue");
	
	
	cnn.queue('profile_queue', function(q){
		q.subscribe(function(message, headers, deliveryInfo, m){
			util.log(util.format( deliveryInfo.routingKey, message));
			util.log("Message: "+JSON.stringify(message));
			util.log("DeliveryInfo: "+JSON.stringify(deliveryInfo));
			
			if(message.apiId==="API1"){
				profileInfo.getProfileData_handle_request(message, function(err,res){

				//return index sent
				cnn.publish(m.replyTo, res, {
					contentType:'application/json',
					contentEncoding:'utf-8',
					correlationId:m.correlationId
				});
			});}
			
			if(message.apiId==="API2"){
				profileInfo.getAllTweets_handle_request(message, function(err,res){

				//return index sent
				cnn.publish(m.replyTo, res, {
					contentType:'application/json',
					contentEncoding:'utf-8',
					correlationId:m.correlationId
				});
			});}

	});
});
	
	
	
	
	
	
});